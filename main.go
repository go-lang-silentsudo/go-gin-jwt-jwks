package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func securedApi(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, gin.H{"message": "Secure API"})
}

func insecureApi(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, gin.H{"message": "Anonymous access API"})
}

func main() {
	router := gin.Default()
	router.Use(Logger())

	secure := router.Group("/secure")
	secure.Use(JwtVerifier(""))
	secure.GET("/", securedApi)

	anonymous := router.Group("anonymous")
	anonymous.GET("/", insecureApi)

	router.Run("localhost:8080")

}
